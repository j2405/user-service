package com.jewellery.userservice.adapters;

import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.entities.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserAdapterTest {

  @Test
  public void testFromRegistrationBeanToUserConversion() {
    var user = UserAdapter.registerBeanToUser(
        RegisterUserBean.builder().lastName("Dimitrov").firstName("Velin").email("test@abv.bg")
            .build());
    Assertions.assertThat(user)
        .isEqualTo(
            User.builder().lastName("Dimitrov").firstName("Velin").email("test@abv.bg").build());
  }
}
