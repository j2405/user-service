package com.jewellery.userservice.endpoints;


import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.dtos.UserAuthentication;
import com.jewellery.userservice.services.interfaces.AuthenticationService;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@ExtendWith(MockitoExtension.class)
public class AuthenticationEndpointTest {

  @Mock
  private AuthenticationService authenticationService;

  @InjectMocks
  private AuthenticationEndpoint endpoint;

  @Test
  public void testValidLogin() {
    var responseEntity = endpoint
        .login(UserAuthentication.builder().password("12345").email("test@abv.bg").build());

    Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    Mockito.verify(authenticationService).login(Mockito.anyString(), Mockito.anyString());
  }

  @Test
  public void testInvalidLoginMissingEmail() {
    var responseEntity = endpoint
        .login(UserAuthentication.builder().password("12345").build());

    Assertions.assertThat((List<String>) responseEntity.getBody()).hasSize(1)
        .contains("Email is required!");
    Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    Mockito.verify(authenticationService, Mockito.never())
        .login(Mockito.anyString(), Mockito.anyString());
  }

  @Test
  public void testInvalidLoginMissingPassword() {
    var responseEntity = endpoint
        .login(UserAuthentication.builder().email("test@abv.bg").build());

    Assertions.assertThat((List<String>) responseEntity.getBody()).hasSize(1)
        .contains("Password is required!");
    Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    Mockito.verify(authenticationService, Mockito.never())
        .login(Mockito.anyString(), Mockito.anyString());
  }

  @Test
  public void testLoginThrowsException() {
    var responseEntity = endpoint
        .login(UserAuthentication.builder().password("12345").email("test@abv.bg").build());

    Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    Mockito.verify(authenticationService).login(Mockito.anyString(), Mockito.anyString());
  }


  @Test
  public void testValidRegister() {
    var responseEntity = endpoint
        .register(RegisterUserBean.builder().email("test@abv.bg").password("12345")
            .passwordConfirm("12345").firstName("dsad").lastName("432423").build());

    Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    Mockito.verify(authenticationService).register(Mockito.any(RegisterUserBean.class));
  }

  @Test
  public void testInValidRegisterNoEmail() {
    var responseEntity = endpoint
        .register(
            RegisterUserBean.builder().password("12345").passwordConfirm("12345").firstName("dsad")
                .lastName("432423").build());

    Assertions.assertThat((List<String>) responseEntity.getBody()).hasSize(1)
        .contains("Email is required!");
    Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    Mockito.verify(authenticationService, Mockito.never())
        .register(Mockito.any(RegisterUserBean.class));
  }
}
