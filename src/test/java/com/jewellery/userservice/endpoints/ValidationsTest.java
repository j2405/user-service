package com.jewellery.userservice.endpoints;

import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.dtos.UserAuthentication;
import com.jewellery.userservice.services.interfaces.AuthenticationService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

public class ValidationsTest {

  @Mock
  private AuthenticationService authenticationService;

  private AuthenticationEndpoint endpoint;

  @BeforeEach
  public void setUp() {
    endpoint = new AuthenticationEndpoint(authenticationService);
  }

  @Test
  public void testValidate_AllRequiredChecks() {

    var checks = endpoint.validate(RegisterUserBean.builder().build());
    Assertions.assertThat(checks).hasSize(5)
        .contains("Email is required!", "Confirm password is required!", "Password is required!",
            "Last name is required!", "First name is required!");
  }

  @Test
  public void testValidate_PasswordsDoNotMatch() {

    var checks = endpoint.validate(RegisterUserBean.builder().email("test@abv.bg").password("12345").passwordConfirm("432").firstName("dsad").lastName("432423").build());
    Assertions.assertThat(checks).hasSize(1)
        .contains("Passwords should match!");
  }

  @Test
  public void testValidate_InvalidMail() {
    var checks = endpoint.validate(RegisterUserBean.builder().email("test@test").password("12345").passwordConfirm("12345").firstName("dsad").lastName("432423").build());
    Assertions.assertThat(checks).hasSize(1)
        .contains("Email is invalid!");
  }

  @Test
  public void testValidateLoginRequiredFields_EmptyMail() {
    var checks = endpoint.requiredLoginFieldsValidation(UserAuthentication.builder().password("12345").build());
    Assertions.assertThat(checks).hasSize(1)
        .contains("Email is required!");
  }

  @Test
  public void testValidateLoginRequiredFields_EmptyPassword() {

    var checks = endpoint.requiredLoginFieldsValidation(UserAuthentication.builder().email("test@abv.bg").build());
    Assertions.assertThat(checks).hasSize(1)
        .contains("Password is required!");
  }


  @Test
  public void testValidateLoginRequiredFields_BothEmptyFields() {
    var checks = endpoint.requiredLoginFieldsValidation(UserAuthentication.builder().build());
    Assertions.assertThat(checks).hasSize(2)
        .contains("Email is required!","Password is required!");
  }

}
