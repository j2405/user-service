package com.jewellery.userservice.services;

import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.endpoints.AuthenticationEndpoint;
import com.jewellery.userservice.entities.User;
import com.jewellery.userservice.exceptions.AuthenticationException;
import com.jewellery.userservice.repositories.UserRepository;
import com.jewellery.userservice.services.interfaces.AuthenticationService;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private AuthenticationServiceImpl authenticationService;

  @Test
  public void testValidLogin() {
    Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(Optional
        .of(User.builder().salt("d30393bbe9382cef383fa884fceddec8")
            .password("6bd4d0a6f75a280b70e7bf0cc71f6c7b264526be8da93c935be6d6496a1897").build()));
    authenticationService.login("test@abv.bg", "NDMyNDIzNA==");
  }

  @Test
  public void testThrowExceptionOnNotFoundUser() {
    Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.empty());
    AuthenticationException exception = Assertions.assertThrows(AuthenticationException.class,
        () -> authenticationService.login("test@abv.bg", "asdfdsa"));
    Assertions
        .assertEquals(exception.getMessage(), "Such user does not exist with this combination!");
  }

  @Test
  public void testThrowExceptionOnNotEqualPasswords() {
    Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(Optional
        .of(User.builder().salt("d30393bbe9382cef383fa884fceddec8").password("NDMyNDIzNA==")
            .build()));
    AuthenticationException exception = Assertions.assertThrows(AuthenticationException.class,
        () -> authenticationService.login("test@abv.bg", "MTIzNDU="));
    Assertions
        .assertEquals(exception.getMessage(), "Such user does not exist with this combination!");
  }

  @Test
  public void testValidRegistration() {
    authenticationService.register(
        RegisterUserBean.builder().email("test@abv.bg").firstName("velin").lastName("Dimitrov")
            .password("12345").passwordConfirm("12345").build());

    Mockito.verify(userRepository).save(Mockito.any(User.class));
  }
}
