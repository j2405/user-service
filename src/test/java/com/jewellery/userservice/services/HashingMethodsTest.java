package com.jewellery.userservice.services;

import com.jewellery.userservice.exceptions.AuthenticationException;
import com.jewellery.userservice.repositories.UserRepository;
import com.jewellery.userservice.services.interfaces.AuthenticationService;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HashingMethodsTest {
  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private AuthenticationServiceImpl authenticationService;

  @Test
  public void testValidHashPasword() {
   String string = authenticationService.hashPassword("12345", getRandomSalt());
   Assertions.assertNotNull(string);
  }


  public byte[] getRandomSalt() {
    SecureRandom random = new SecureRandom();
    byte[] salt = new byte[16];
    random.nextBytes(salt);
    return salt;
  }
}
