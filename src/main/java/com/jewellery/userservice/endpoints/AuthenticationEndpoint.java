package com.jewellery.userservice.endpoints;

import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.dtos.UserAuthentication;
import com.jewellery.userservice.exceptions.AuthenticationException;
import com.jewellery.userservice.services.interfaces.AuthenticationService;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthenticationEndpoint {

  private final AuthenticationService authenticationService;

  @PostMapping("/login")
  public ResponseEntity<?> login(@RequestBody UserAuthentication userAuthentication) {
    List<String> errorMessages =  requiredLoginFieldsValidation(userAuthentication);
    if (!errorMessages.isEmpty()) {
      return ResponseEntity.badRequest().body(errorMessages);
    }

    try {
      return ResponseEntity.ok(authenticationService.login(userAuthentication.getEmail(), userAuthentication.getPassword()));
    } catch (AuthenticationException e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }

  }

  @PostMapping("/register")
  public ResponseEntity<?> register(@RequestBody RegisterUserBean bean) {
    List<String> errorMessages = validate(bean);
    if (!errorMessages.isEmpty()) {
      return ResponseEntity.badRequest().body(errorMessages);
    }
    authenticationService.register(bean);
    return ResponseEntity.ok().build();
  }

  List<String> validate(RegisterUserBean bean) {
    List<String> errorMessages = requiredFieldsValidation(bean);

    if (!errorMessages.isEmpty()) {
      return errorMessages;
    }

    if (StringUtils.isNotBlank(bean.getPassword()) && StringUtils
        .isNotBlank(bean.getPasswordConfirm()) && !bean.getPassword()
        .equals(bean.getPasswordConfirm())) {
      errorMessages.add("Passwords should match!");
    }
    if (!EmailValidator.getInstance().isValid(bean.getEmail())) {
      errorMessages.add("Email is invalid!");
    }

    return errorMessages;
  }

  List<String> requiredFieldsValidation(RegisterUserBean bean) {
    List<String> errorMessages = new ArrayList<>();
    if (StringUtils.isBlank(bean.getFirstName())) {
      errorMessages.add("First name is required!");
    }
    if (StringUtils.isBlank(bean.getLastName())) {
      errorMessages.add("Last name is required!");
    }
    if (StringUtils.isBlank(bean.getPassword())) {
      errorMessages.add("Password is required!");
    }
    if (StringUtils.isBlank(bean.getPasswordConfirm())) {
      errorMessages.add("Confirm password is required!");
    }
    if (StringUtils.isBlank(bean.getEmail())) {
      errorMessages.add("Email is required!");
    }
    return errorMessages;
  }

  List<String> requiredLoginFieldsValidation(UserAuthentication userAuthentication) {
    List<String> errorMessages = new ArrayList<>();
    if (StringUtils.isBlank(userAuthentication.getEmail())) {
      errorMessages.add("Email is required!");
    }
    if (StringUtils.isBlank(userAuthentication.getPassword())) {
      errorMessages.add("Password is required!");
    }

    return errorMessages;
  }

  @GetMapping("/test")
  public String test() {

    return "test";
  }
}
