package com.jewellery.userservice.services;

import com.jewellery.userservice.adapters.UserAdapter;
import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.entities.User;
import com.jewellery.userservice.exceptions.AuthenticationException;
import com.jewellery.userservice.repositories.UserRepository;
import com.jewellery.userservice.services.interfaces.AuthenticationService;
import com.jewellery.userservice.util.JwtTokenUtil;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

  public static final String HASHING_ALGORITHM = "PBKDF2WithHmacSHA512";
  public static final int ITERATION_COUNT = 1000;
  public static final int KEY_LENGTH = 255;
  private final UserRepository userRepository;
  private final JwtTokenUtil jwtTokenUtil;

  @Override
  public String login(String email, String passwordBase64) {
    User user = userRepository.findByEmail(email).orElseThrow(
        () -> new AuthenticationException("Such user does not exist with this combination!"));

    byte[] salt = decodeHex(user.getSalt());

    if (!user.getPassword().equals(hashPassword(passwordBase64, salt))) {
      throw new AuthenticationException("Such user does not exist with this combination!");
    }

    return jwtTokenUtil.generateToken(user);
  }

  @Override
  public void register(RegisterUserBean dto) {
    SecureRandom random = new SecureRandom();
    byte[] salt = new byte[16];
    random.nextBytes(salt);

    User user = UserAdapter.registerBeanToUser(dto);
    user.setSalt(encodeHex(salt));
    user.setPassword(hashPassword(dto.getPassword(), salt));
    userRepository.save(user);

  }


  String hashPassword(final String password, final byte[] salt) {
    try {
      SecretKeyFactory skf = SecretKeyFactory.getInstance(HASHING_ALGORITHM);
      PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
      SecretKey key = skf.generateSecret(spec);
      byte[] res = key.getEncoded();
      return encodeHex(res);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new AuthenticationException(e.getMessage());
    }
  }

  String encodeHex(final byte[] bytesToEncode) {
    return Hex.encodeHexString(bytesToEncode);
  }

  byte[] decodeHex(final String encodedBytes) {
    try {
      return Hex.decodeHex(encodedBytes);
    } catch (DecoderException e) {
      e.printStackTrace();
    }
    throw new IllegalArgumentException("Error while decoding");
  }

}
