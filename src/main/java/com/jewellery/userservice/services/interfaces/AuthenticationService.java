package com.jewellery.userservice.services.interfaces;

import com.jewellery.userservice.dtos.RegisterUserBean;

public interface AuthenticationService {

  String login(String email,String passwordBase64);

  void register(RegisterUserBean dto);
}
