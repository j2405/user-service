package com.jewellery.userservice.adapters;

import com.jewellery.userservice.dtos.RegisterUserBean;
import com.jewellery.userservice.entities.User;

public class UserAdapter {

  public static User registerBeanToUser(RegisterUserBean dto) {
    return User.builder().email(dto.getEmail())
        .firstName(dto.getFirstName()).lastName(dto.getLastName()).build();
  }
}
