package com.jewellery.userservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUserBean {
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private String passwordConfirm;
}
